package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv" // for turning user input into an int
	"strings"
	"time"
)

// this is the main loop where all the 'music' happens
func main() {

	printProgramHeader()
	tempo := getUserTempo()
	duration := getUserDuration()

	fmt.Printf("Tempo selected: %v BPM\n\n", tempo)

	// calcluate notes & intervals in the song (one bar worth of)
	notes := map[string][]int{
		"kick":   {1, 3, 5, 7, 9, 11, 13, 15},
		"snare":  {5, 13},
		"hi hat": {3, 7, 11, 15},
	}

	// starts the metronme with the params (tempo, steps/bar, beats/bar, notes)
	startSong(duration, float64(tempo), 8, 4, notes)
}

// printProgramHeader() prints the start of the program
func printProgramHeader() {
	fmt.Println("----------------------------------")
	fmt.Println("Welcome to the SM-808 Beat Machine")
	fmt.Println("----------------------------------")
	fmt.Println()
	fmt.Println("Got beats? Lets hear em!")
	fmt.Println()
}

// getUserTempo() gets user input for the song tempo and start the song
func getUserTempo() int {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Print("Please enter a tempo for the song (in BPM): ")
	userInput, _ := inputReader.ReadString('\n')
	tempoInput := strings.TrimSuffix(userInput, "\n")

	// convert to a int and return (so we can do music maths)
	userTempo, err := strconv.Atoi(tempoInput)
	if err != nil {
		fmt.Println()
		fmt.Println("Please enter a valid integer for the tempo. Try again")
		fmt.Println()
		log.Fatal(err)
	}

	return userTempo
}

// getUserDuration() gets user input for the song tempo and start the song
func getUserDuration() int {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Print("Please enter a duration for the song (in secs): ")
	userInput, _ := inputReader.ReadString('\n')
	durationInput := strings.TrimSuffix(userInput, "\n")

	// convert to a int and return (so we can do music maths)
	songDuration, err := strconv.Atoi(durationInput)
	if err != nil {
		fmt.Println()
		fmt.Println("Please enter a valid integer for the duration. Try again")
		fmt.Println()
		log.Fatal(err)
	}

	return songDuration
}

// send to note queue
func sendNotes(playQueue chan<- string, note string) {
	playQueue <- note
}

// recieve the note, and print it
func recvNotes(playQueue <-chan string) string {
	// instead of one, recv ALL the notes from the channel, place in array, and pretty print in note syntax (ex: [kick+snare]...[])

	return <-playQueue
	// fmt.Printf("[%v]...", note)
}

// calculateBeat() goes through the notes array and calculates beat to be played and outputs them as a string
func calculateBeat(stepCount int, notes map[string][]int, playQueue chan<- string) {
	// for the beat keep track of all the notes to be played
	// beatNotes := []string{}
	beatNotes := make([]string, 0)

	for note, steps := range notes {
		// for each note, if the note is supposed to played, append note name to beatNotes
		if stepsContains(steps, stepCount) {
			beatNotes = append(beatNotes, note)
		}
	}

	// if at the end of counting we have our array, concat the contents and send to the play queue channel
	if len(beatNotes) > 0 {
		// concat and sendNotes()
		noteString := strings.Join(beatNotes, "+")
		sendNotes(playQueue, noteString)
	}
}

// contains searches for a int in a slice of ints
func stepsContains(haystack []int, needle int) bool {
	// ignore the keys and look for the needle
	for _, num := range haystack {
		if needle == num {
			return true
		}
	}
	return false
}

// startSong starts tickers to keep rhythm (takes in song tempo and stepsPerBar in # of steps)
func startSong(duration int, songTempo float64, stepsPerBar int, beatsPerBar int, notes map[string][]int) {

	// calculate rhythm timing
	secsPerBeat := (60.0 / float64(songTempo))
	secsPerBar := secsPerBeat * float64(beatsPerBar)
	secsPerStep := secsPerBar / float64(stepsPerBar)

	// in order to play
	barCount := 1
	stepCount := 0
	beatCount := 0

	// playQueue is a channel for send the notes to be played
	playQueue := make(chan string, 1)

	fmt.Println()
	fmt.Println("§ - Bar", barCount)

	// sets a clock to time steps and a metronome to keep the beat
	clock, metronome :=
		time.NewTicker(time.Duration(secsPerStep*1000)*time.Millisecond),
		time.NewTicker(time.Duration(secsPerBeat*1000)*time.Millisecond)

	// handle the steps, [important that we start this before the music]
	go func() {

		for range clock.C {
			// count a step
			stepCount++

			// determine whether a note will be played
			calculateBeat(stepCount, notes, playQueue)

			// see if we hit the end of the bar
			if stepCount%stepsPerBar == 0 {
				// if so, reset (we reached ONE BAR)
				stepCount = 0
			}
		}
	}()

	// handle / play all the beats
	go func() {

		for range metronome.C { // for m := range metronome.C {
			// count a beat
			beatCount++

			// see if we hit the end of the bar
			if beatCount%beatsPerBar == 0 {
				// if so.. play note w/o the ending '...'
				fmt.Printf("[%v]", recvNotes(playQueue))

				// print the next line
				barCount++ // count an actual completed bar
				fmt.Println()
				fmt.Println()
				fmt.Println("§ - Bar", barCount)

				// else.. play a beat
			} else {
				fmt.Printf("[%v]...", recvNotes(playQueue))
			}

		}
	}()

	// song duration [TODO: make this sync up with the step count to end on beat]
	time.Sleep(time.Duration(duration) * time.Second) // play the song
	clock.Stop()
	metronome.Stop()

	// wait a second and print final output
	time.Sleep(1000 * time.Millisecond)
	printProgramFooter(duration, barCount, notes)

}

// printProgramFooter() prints the final footer output
func printProgramFooter(duration int, barCount int, notes map[string][]int) {
	fmt.Println()
	fmt.Println()
	fmt.Println("Song stopped.")
	fmt.Println("Duration:", duration, "secs")
	fmt.Println("Bars Played:", (barCount - 1))

	fmt.Println()
	fmt.Println("NOTES OF SONG")
	fmt.Println("-----")
	for note := range notes {
		fmt.Println(note)
	}

	fmt.Println()
	fmt.Println("§ Thanks for playing §")
}
